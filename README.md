# Views Delimited List

This module adds a new display style to Views, to show the results of a view as a comma-separated list.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Edit the view that you want to display as a delimited list.
2. Configure the display format:
    1. Change "Format" to be "Delimited text list". 
    2. Customize your style by click on "Settings" next to "Delimited text list". 

       Note that, in order for the list items to be properly formatted inline, the fields displayed must be inline.
